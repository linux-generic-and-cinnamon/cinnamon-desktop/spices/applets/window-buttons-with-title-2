Window buttons with title 2 - Cinnamon applet

Places titlebar buttons and window title in the panel when window is maximized, removing the titlebar.

![](screenshot.png)

